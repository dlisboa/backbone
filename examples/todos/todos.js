// É necessário esse JSON?  Podemos fazer updates parcials de cada objeto (row,
// widget, etc) e não precisaríamos gravar no JSON ou LocalStorage.  O problema
// é que acabamos um pouco com a idéia de offline editing.  Também há uma certa
// complexidade.  Porém com updates parcials podemos fazer "undo infinito" e
// outras coisas que não vem à mente agora.
//
//  Com LocalStorage seria mais simples e atômico o ato de "salvar" a página.
//  Teríamos muito menos requests para lidar desta forma.
//
// Essa estrutura representa o HTML que é retornado pelo servidor, uma forma de
// "ensinar" a aplicação JS (o código Backbone) o significado dos elementos HTML
// (as divs, etc).

var json = {
  page : {
    rows : [
      {
        id : "50f4752e8e9bef18da3b63c5",
        name : "Row",
        regions: [
          {
            id : "e9bef150f4752e88da3b63d4",
            name: "Region",
            widget : {
              id : "3b63c50f4752e8e9bef18daa",
              name : "Widget",
              type : "image",
              content : "<p>foobar</p>"
            }
          }
        ]
      }
    ]
  }
};

// Se os HTML fosse auto-descritivo (ou seja, carregasse com ele os IDs de cada
// object) talvez não precisaríamos deste JSON. A aplicação JS poderia pedir ao
// servidor por informações (fazer GETs) e executar ações (POST/PUT).
//
// Para fazer isso poderíamos construir um endpoint para o qual mandamos um ID
// (o id do objet) e ele retorna a URL deste objeto. Então, faríamos um request
// em cima desta URL (GET ou POST ou PUT).
//
// Isso faría com que nós não precisássemos "chumbar" a URL nos Backbone.Models,
// por exemplo, falar que o model Widget faz PUT para "/widgets/:id". A URL
// seria retornada pelo servidor e o código que fáz o PUT seria praticamente o
// mesmo para todos os modelos.

var SidebarView = Backbone.View.extend({
  el: $('section'),
  events: {
    'dragstart div#widget-text': 'dragStart',
    'drop div.droppable': 'dragDrop',
    'dragover div.droppable': function(){return false;}
  },

  dragStart: function(ev){
    // o evento é 
    // store data-type temporarely
    ev.originalEvent.dataTransfer.setData('type', $(ev.target).attr('data-type'));
  },

  dragDrop: function(event){
    var region = $(event.target);
    var type = event.originalEvent.dataTransfer.getData('type');

    // Este é o código que faz update do JSON local (na memória e LocalStorage).
    // Na verdade não fuciona só descomentado, nós mudamos certas coisas no HTML
    // e ele precisa ser modificado, mas a idéia é modificar o JSON para que
    // adicione uma Widget à sua lista de widgets relacionadas à página.

    // var widget = {
    //   name: "Widget",
    //   type: event.originalEvent.dataTransfer.getData('type')
    // };

    // actualRow = _.find(json.page.rows, function(row) {
    //   if (row.id == dropRow.attr('data-row-id')) {
    //     return row;
    //   }
    // });
    // actualRow.widgets.push(newWidget);

    // O código da widget, aquilo que devemos injetar no HTML para que apareça
    // na tela (por exemplo o editor inline), precisa estar em algum lugar.
    // Poderíamos pegar este código direto do servidor, mas como ele é sempre o
    // mesmo uma solução que encontramos foi enviar este código junto com o HTML
    // do editor, em tags <script> com um "type" diferente (ver index.html).
    template = $('#widget-' + type + '-template').html();
    region.html(template);
    // Aonde inserir este HTML também é outra coisa relevante: vamos colocar em
    // regions? Vamos deixar as widgets soltas em rows? O bom da segunda idéia
    // é que reduz complexidade da modelagem (uma página tem rows e elas tem
    // widgets, somente isso). O problema é que a ordenação das widgets nas
    // rows seriam responsabilidade delas. As regions assumem basicamente essa
    // responsabilidade, mas fazem com que existam regiões fixas onde se pode
    // inserir as widgets. Se as regions não forem regiões fixas, então por que
    // elas existem? A idéia de rows, regions e widgets é mapear a posição dos
    // objetos para linhas e colunas (x, y). As rows mapeiam x e outra coisa
    // mapeia y (a ordem das coisas dentro das colunas).
  }
});
var sidebarView = new SidebarView();
